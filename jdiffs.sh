wdiff -w '<span class="added-text">' -x '</span>' -y '<strike>' -z '</strike>' ~/jdiffs/final.md ~/jdiffs/original.md > ~/jdiffs/edit.md

echo -e "# Final version (reflecting my edits)\n" > combined.md
cat final.md >> combined.md

echo -e "\n# The changes I made\n" >> combined.md
cat edit.md >> combined.md

echo -e "\n# Your original version for reference\n" >> combined.md
cat original.md >> combined.md

multimarkdown combined.md > combined.html

cat jdiffs-html-css-head-1.html jdiffs.css jdiffs-html-css-head-2.html combined.html jdiffs-html-body-closer.html > index.html