4 Ideas for Event Photo Capture
 
Go High Tech! Capture Futuristic Photos with a Camera Array
 
At a recent sponsored event, attendees lined up at HP's booth to have their photo taken by the FotoZap Camera Array. As each group of guests jumped in the air, 25 professional cameras all fired at the same instant. The resulting image froze the moment in time and rotated it in space.

 
Watch FotoZap Camera Array Video - freeze time and rotate it
View the video: the FotoZap Camera Array
 
Participants viewed their one-of-a-kind image online and then shared it as branded social media. High tech setups like Camera Arrays draw attention to the sponsor's event footprint and turn an attendee into a brand champion--someone who will share their branded photo on Facebook and talk about their experience with their friends.

 
Example of Camera Array image
Let us recommend the right photo capture technology for your event footprint.

 

Start Simple! Smart devices transform any staff member into an event photographer
 
With FotoZap installed on a tablet, phone, or connected camera, an event staff member can now walk through the crowd, snapping photos or videos of attendees and immediately delivering a digital copy of the image by: typing in the guest's email address, or handing out a card or printing a glossy photo that contains a unique URL.

 
Run the FotoZap app on an array of smart devices
 
Whether an event attendee claims their photo from home, on their smart phone, or on an event scanning kiosk, they first fill out a lead generation form that builds your list. And because each photo or video is enhanced with branded graphics from the event, attendees enjoy sharing the photos with their friends.

 
Get your own branded Picture Marketing app today. All you need is a logo to start.

 

Get Creative! Transform Photos and Videos with 1000s of Special-Effects Apps
 
Want elaborate special effects for your event photos without an elaborate setup? Event staff can apply amazing special effects to captured photos and videos, and then use FotoZap to deliver the image to the event attendees.

 
Watch the video on how to use iOS and Android image apps with FotoZap
 
Staff members alter the image using any photo or video app on iOS or Android, and then the FotoZap app brands the image and turns it into shareable social media. Morph two faces into one, create a nostalgic photo, blow people up with rockets, add attendees into cartoon, etc.

 
Watch the video to see how to use any image app with FotoZap.

 

Get Physical! Construct a Scene with Props and a Movie Set
 
More and more, event holders are building elaborate physical backdrops, almost like movie sets, rather than relying on lifeless green screens. At one recent event, attendees stood next to a crowd of mannequins holding protest signs. At another, attendees milked a full-sized plastic cow.

 
Construct a scene with props and a movie set
 
Let us help you put together the perfect mix of digital effects and physical props.

 

Talk to us for more photo capture ideas
 
Click on the links above, reply to this email, call me at (949) 429-3030, or fill out this form.
