# Final version (reflecting my edits)

**what about "Top 4 Ways to Capture Event Photos"?**
4 Ideas for Event Photo Capture



Enter the Matrix! Capture Futuristic Photos with a Camera Array
 
At a recent sponsored event, attendees lined up at HP's booth to have their photo taken by a high-tech setup called the FotoZap Camera Array. As each group of guests jumped in the air, 25 professional cameras all fired at the same instant. The resulting image froze the moment in time and rotated it in space.

 
Watch FotoZap Camera Array Video - freeze time and rotate it
View the video: the FotoZap Camera Array
 
Participants were eager to see themselves in "bullet-time," the same technology used in The Matrix, and share it as branded social media. High-tech setups like Camera Arrays draw attention to the sponsor's event footprint and turn an attendee into a brand champion--someone who will share their branded photo on Facebook and talk about their experience with their friends.

 
Example of Camera Array image
Let us recommend the right photo capture technology for your event footprint.

 

Start Simple! Smart devices transform any staff member into an event photographer
 
With FotoZap installed on a tablet, phone, or connected camera, an event staff member can now walk through the crowd, snapping photos or videos of attendees and immediately delivering a digital copy of the image by typing in the guest's email address, printing a glossy photo, or handing out a card with a unique URL.

 
Run the FotoZap app on an array of smart devices
 
To view their photo, you require them to first fill out a lead generation form. It's win-win: attendees get an photo enhanced with branded graphics from the event that they'll want to share with their friends, and you get to build your list.

 
Get your own branded Picture Marketing app today. All you need is a logo to start.

 

Get Creative! Transform Photos and Videos with Thousands of Special-Effects Apps
 
Want elaborate special effects for your event photos without an elaborate setup? Event staff can apply amazing special effects to captured photos and videos, then use FotoZap to deliver the image to the event attendees.

 
Watch the video on how to use iOS and Android image apps with FotoZap
 
Staff members can alter the image using any photo or video app on iOS or Android, then the FotoZap app brands the image and turns it into shareable social media. Morph two faces into one, create a nostalgic photo, blow people up with rockets, add attendees into a cartoon, and lots of other fun effects.

 
Watch the video to see how to use any image app with FotoZap.

 

Get Physical! Construct a Scene with Props and a Movie Set
 
More and more, event holders are building elaborate physical backdrops, almost like movie sets, rather than relying on lifeless green screens. At one recent event, attendees stood next to a crowd of mannequins holding protest signs. At another, attendees milked a full-sized plastic cow.

 
Construct a scene with props and a movie set
 
Let us help you put together the perfect mix of digital effects and physical props.

 

Talk to us for more photo capture ideas
 
Click on the links above, reply to this email, call me at (949) 429-3030, or fill out this form.

# The changes I made

<span class="added-text">**what about "Top 4 Ways to Capture Event Photos"?**</span>4 Ideas for Event Photo Capture



<span class="added-text">Enter the Matrix!</span>
 
<strike>Go High Tech!</strike> Capture Futuristic Photos with a Camera Array
 
At a recent sponsored event, attendees lined up at HP's booth to have their photo taken by <span class="added-text">a high-tech setup called</span> the FotoZap Camera Array. As each group of guests jumped in the air, 25 professional cameras all fired at the same instant. The resulting image froze the moment in time and rotated it in space.

 
Watch FotoZap Camera Array Video - freeze time and rotate it
View the video: the FotoZap Camera Array
 
Participants <span class="added-text">were eager to see themselves in "bullet-time," the same technology used in The Matrix,</span> <strike>viewed their one-of-a-kind image online</strike> and <span class="added-text">share</span> <strike>then shared</strike> it as branded social media. <span class="added-text">High-tech</span> <strike>High tech</strike> setups like Camera Arrays draw attention to the sponsor's event footprint and turn an attendee into a brand champion--someone who will share their branded photo on Facebook and talk about their experience with their friends.

 
Example of Camera Array image
Let us recommend the right photo capture technology for your event footprint.

 

Start Simple! Smart devices transform any staff member into an event photographer
 
With FotoZap installed on a tablet, phone, or connected camera, an event staff member can now walk through the crowd, snapping photos or videos of attendees and immediately delivering a digital copy of the image <span class="added-text">by</span> <strike>by:</strike> typing in the guest's email address, <span class="added-text">printing a glossy photo,</span> or handing out a card <span class="added-text">with</span> <strike>or printing a glossy photo that contains</strike> a unique URL.

 
Run the FotoZap app on an array of smart devices
 
<span class="added-text">To view</span>
 
<strike>Whether an event attendee claims</strike> their <span class="added-text">photo, you require them to</span> <strike>photo from home, on their smart phone, or on an event scanning kiosk, they</strike> first fill out a lead generation <span class="added-text">form. It's win-win: attendees get an</span> <strike>form that builds your list. And because each</strike> photo <strike>or video is</strike> enhanced with branded graphics from the <span class="added-text">event that they'll want to share</span> <strike>event, attendees enjoy sharing the photos</strike> with their <span class="added-text">friends, and you get to build your list.</span> <strike>friends.</strike>

 
Get your own branded Picture Marketing app today. All you need is a logo to start.

 

Get Creative! Transform Photos and Videos with <span class="added-text">Thousands</span> <strike>1000s</strike> of Special-Effects Apps
 
Want elaborate special effects for your event photos without an elaborate setup? Event staff can apply amazing special effects to captured photos and videos, <strike>and</strike> then use FotoZap to deliver the image to the event attendees.

 
Watch the video on how to use iOS and Android image apps with FotoZap
 
Staff members <span class="added-text">can</span> alter the image using any photo or video app on iOS or Android, <strike>and</strike> then the FotoZap app brands the image and turns it into shareable social media. Morph two faces into one, create a nostalgic photo, blow people up with rockets, add attendees into <span class="added-text">a</span> cartoon, <span class="added-text">and lots of other fun effects.</span> <strike>etc.</strike>

 
Watch the video to see how to use any image app with FotoZap.

 

Get Physical! Construct a Scene with Props and a Movie Set
 
More and more, event holders are building elaborate physical backdrops, almost like movie sets, rather than relying on lifeless green screens. At one recent event, attendees stood next to a crowd of mannequins holding protest signs. At another, attendees milked a full-sized plastic cow.

 
Construct a scene with props and a movie set
 
Let us help you put together the perfect mix of digital effects and physical props.

 

Talk to us for more photo capture ideas
 
Click on the links above, reply to this email, call me at (949) 429-3030, or fill out this form.

# Your original version for reference

4 Ideas for Event Photo Capture
 
Go High Tech! Capture Futuristic Photos with a Camera Array
 
At a recent sponsored event, attendees lined up at HP's booth to have their photo taken by the FotoZap Camera Array. As each group of guests jumped in the air, 25 professional cameras all fired at the same instant. The resulting image froze the moment in time and rotated it in space.

 
Watch FotoZap Camera Array Video - freeze time and rotate it
View the video: the FotoZap Camera Array
 
Participants viewed their one-of-a-kind image online and then shared it as branded social media. High tech setups like Camera Arrays draw attention to the sponsor's event footprint and turn an attendee into a brand champion--someone who will share their branded photo on Facebook and talk about their experience with their friends.

 
Example of Camera Array image
Let us recommend the right photo capture technology for your event footprint.

 

Start Simple! Smart devices transform any staff member into an event photographer
 
With FotoZap installed on a tablet, phone, or connected camera, an event staff member can now walk through the crowd, snapping photos or videos of attendees and immediately delivering a digital copy of the image by: typing in the guest's email address, or handing out a card or printing a glossy photo that contains a unique URL.

 
Run the FotoZap app on an array of smart devices
 
Whether an event attendee claims their photo from home, on their smart phone, or on an event scanning kiosk, they first fill out a lead generation form that builds your list. And because each photo or video is enhanced with branded graphics from the event, attendees enjoy sharing the photos with their friends.

 
Get your own branded Picture Marketing app today. All you need is a logo to start.

 

Get Creative! Transform Photos and Videos with 1000s of Special-Effects Apps
 
Want elaborate special effects for your event photos without an elaborate setup? Event staff can apply amazing special effects to captured photos and videos, and then use FotoZap to deliver the image to the event attendees.

 
Watch the video on how to use iOS and Android image apps with FotoZap
 
Staff members alter the image using any photo or video app on iOS or Android, and then the FotoZap app brands the image and turns it into shareable social media. Morph two faces into one, create a nostalgic photo, blow people up with rockets, add attendees into cartoon, etc.

 
Watch the video to see how to use any image app with FotoZap.

 

Get Physical! Construct a Scene with Props and a Movie Set
 
More and more, event holders are building elaborate physical backdrops, almost like movie sets, rather than relying on lifeless green screens. At one recent event, attendees stood next to a crowd of mannequins holding protest signs. At another, attendees milked a full-sized plastic cow.

 
Construct a scene with props and a movie set
 
Let us help you put together the perfect mix of digital effects and physical props.

 

Talk to us for more photo capture ideas
 
Click on the links above, reply to this email, call me at (949) 429-3030, or fill out this form.
